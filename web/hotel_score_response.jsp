<%--
  Created by IntelliJ IDEA.
  User: mackr
  Date: 14.04.2020
  Time: 18:39
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<%@ page contentType="text/html;charset=UTF-8" language="java"
         import="lista_6.HotelScore, java.util.*" %>
<%@ page import="lista_6.Metryka" %>


<html>
<head>
    <title>Title</title>
</head>
<body>

<%
HotelScore hotelScore = new HotelScore();
hotelScore.setSrednia_cena_za_pokoj(Double.parseDouble(request.getParameter("srednia_cena_za_pokoj")));
hotelScore.setCalkowita_liczba_pokoi(Integer.parseInt(request.getParameter("calkowita_liczba_pokoi")));
hotelScore.setLiczba_osadzonych_pokojow(Integer.parseInt(request.getParameter("liczba_osadzonych_pokojow")));
hotelScore.setLiczba_analizowanych_dni(Integer.parseInt(request.getParameter("liczba_analizowanych_dni")));
hotelScore.setMetryka(request.getParameter("metryka"));
pageContext.setAttribute("hotel", hotelScore); // atrybut na stronie
%>
<p>Ocena wybranej metryki (${hotel.metryka}) to </br> ${hotel.ocena()} </p>
</body>
</html>
