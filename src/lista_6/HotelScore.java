package lista_6;

public class HotelScore {
    private double srednia_cena_za_pokoj;
    private int calkowita_liczba_pokoi;
    private int liczba_osadzonych_pokojow;
    private int liczba_analizowanych_dni;
    private String metryka;

    public double ocena()throws NumberFormatException{
        double ocena = 0;
        switch (metryka) {
            case "RevPAR":
                ocena =  metrykaRevPAR();
                break;
            case "TRevPAR":
               ocena =  metrykaTRevPAR();
                break;
            case "RevPOR":
                ocena =  metrykaRevPOS();
                break;
        }
        return Math.round(ocena*100)/100;
    }

    private double metrykaRevPOS() {
        double calowity_dochod = srednia_cena_za_pokoj*liczba_osadzonych_pokojow*0.92;
        if (liczba_osadzonych_pokojow == 0)
            return 0;
        else
        return calowity_dochod/liczba_osadzonych_pokojow;
    }

    private double metrykaRevPAR(){
        return srednia_cena_za_pokoj*(double)liczba_osadzonych_pokojow/(double)calkowita_liczba_pokoi*liczba_analizowanych_dni;
    }

    private double metrykaTRevPAR(){
       double calowity_dochod = srednia_cena_za_pokoj*liczba_osadzonych_pokojow*0.92;
       return calowity_dochod/calkowita_liczba_pokoi;
    }

    //kontruktor, get, set


    public HotelScore(double srednia_cena_za_pokoj, int calkowita_liczba_pokoi, int liczba_osadzonych_pokojow, int liczba_analizowanych_dni, String metryka) {
        this.srednia_cena_za_pokoj = srednia_cena_za_pokoj;
        this.calkowita_liczba_pokoi = calkowita_liczba_pokoi;
        this.liczba_osadzonych_pokojow = liczba_osadzonych_pokojow;
        this.liczba_analizowanych_dni = liczba_analizowanych_dni;
        this.metryka = metryka;
    }

    public HotelScore() {
    }

    public double getSrednia_cena_za_pokoj() {
        return srednia_cena_za_pokoj;
    }

    public void setSrednia_cena_za_pokoj(double srednia_cena_za_pokoj) {
        this.srednia_cena_za_pokoj = srednia_cena_za_pokoj;
    }

    public int getCalkowita_liczba_pokoi() {
        return calkowita_liczba_pokoi;
    }

    public void setCalkowita_liczba_pokoi(int calkowita_liczba_pokoi) {
        this.calkowita_liczba_pokoi = calkowita_liczba_pokoi;
    }

    public int getLiczba_osadzonych_pokojow() {
        return liczba_osadzonych_pokojow;
    }

    public void setLiczba_osadzonych_pokojow(int liczba_osadzonych_pokojow) {
        this.liczba_osadzonych_pokojow = liczba_osadzonych_pokojow;
    }

    public int getLiczba_analizowanych_dni() {
        return liczba_analizowanych_dni;
    }

    public void setLiczba_analizowanych_dni(int liczba_analizowanych_dni) {
        this.liczba_analizowanych_dni = liczba_analizowanych_dni;
    }

    public String getMetryka() {
        return metryka;
    }

    public void setMetryka(String metryka) {
        this.metryka = metryka;
    }
}
