package lista_6;

public enum Metryka {
    RevPAR,
    TRevPAR,
    RevPOR;

     Metryka() {
    }

    public Metryka setFromString(String string){
         Metryka metryka = Metryka.RevPAR;
         if
          (string.equals(Metryka.TRevPAR.toString()))
             metryka = Metryka.TRevPAR;
         else
             metryka = Metryka.RevPOR;
         return metryka;
     }
}
